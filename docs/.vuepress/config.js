module.exports = {
  title: "PangElement",
  description: "项目中常用到的组件",
  base: "/pang-element-docs/",
  dest: "dist",
  plugins: ["@vuepress/back-to-top"],
  themeConfig: {
    logo: "/avatar.jpg",
    authorAvatar: "/avatar.jpg",
    lastUpdated: "Last Updated",
    subSidebar: "auto",
    nav: [
      { text: "首页", link: "/" },
      {
        text: "Gitee",
        link: "https://gitee.com/panglehaoya/pang-element",
      },
    ],
    sidebar: [
      {
        title: "使用方式",
        path: "/use/use",
        collapsable: true,
      },
      {
        title: "组件",
        path: "/components/FormTable",
        children: [
          {
            title: "FormTable",
            path: "/components/FormTable",
            collapsable: true,
          },
          {
            title: "DynamicForm",
            path: "/components/DynamicForm",
            collapsable: true,
          },
          {
            title: "MultiLevelMenu",
            path: "/components/MultiLevelMenu",
            collapsable: true,
          },
          {
            title: "ContextMenu",
            path: "/components/ContextMenu",
            collapsable: true,
          },
          {
            title: "ShowMessage",
            path: "/components/ShowMessage",
            collapsable: true,
          },
          {
            title: "DeptForm",
            path: "/components/DeptForm",
            collapsable: true,
          },
          {
            title: "Upload",
            path: "/components/Upload",
            collapsable: true,
          },
        ],
      },
    ],
  },
};
