import Element from 'element-ui'
import PangElement from 'pang-element'
import hljs from 'highlight.js';

import 'element-ui/lib/theme-chalk/index.css'
import 'pang-element/lib/pang-element.css'
import "highlight.js/styles/atom-one-dark.css";

export default ({ Vue }) => {
  Vue.use(Element)
  Vue.use(PangElement)
  Vue.directive('highlight', function (el) {
    let blocks = el.querySelectorAll('pre code');
    blocks.forEach((block)=>{
      hljs.highlightBlock(block);
    })
  });
};
