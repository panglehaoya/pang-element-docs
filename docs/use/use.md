## 安装

<CodeCopy copyContainerId="installCopyContainerId" copyId="installCopyId">
:::slot highlight
```js
npm i pang-element
```
:::
</CodeCopy>

## 使用

<CodeCopy copyContainerId="useCopyContainerId" copyId="useCopyId">
:::slot highlight
```js
import PangElement from 'pang-element'
import 'pang-element/lib/pang-element.css'

Vue.use(PangElement)
```
:::
</CodeCopy>


