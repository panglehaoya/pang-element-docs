# ContextMenu

<Title title="介绍"></Title>

单例模式下的message box


<Title title="组件的使用"></Title>


<demo-block>
:::slot source
<ShowMessageDemo></ShowMessageDemo>
:::

:::slot highlight
```html
<template>
    <div>
      <el-button @click="handleClick('success')" type="success">
          show message
      </el-button>
      <el-button @click="handleClick('info')" type="info">show message</el-button>
      <el-button @click="handleClick('warning')" type="warning">
          show message
      </el-button>
      <el-button @click="handleClick('error')" type="danger">
          show message
      </el-button>
    </div>
</template>

<script>
  import PangElement from "pang-element";

  export default {
    name: "ShowMessageDemo",
    methods: {
      handleClick(type) {
        const msg = new PangElement.ShowMessage();
        msg[type]("show message");
      },
    },
  };
</script>
```
:::
</demo-block>

<Title title="Attributes"></Title>
与el-message的参数一致
