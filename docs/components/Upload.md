# ContextMenu

<Title title="介绍"></Title>

用于需要校验文件的上传组件

<Title title="组件的使用"></Title>

<demo-block>
:::slot source
<UploadDemp></UploadDemp>
:::

传递校验函数和上传的api即可

校验函数返回false不会上传

:::slot highlight
```html
<template>
  <div>
    <PUpload
      :validate-fn="validateFn"
      :upload-api="uploadApi"
      tips="请上传jpg文件"
    />
  </div>
</template>

<script>
  export default {
    methods: {
      validateFn(file) {
        const fileSuffix = file.name.split(".")[1];
        const accept = ["jpg"];
        return accept.includes(fileSuffix);
      },
      uploadApi() {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve("success");
            this.$message.success("上传成功");
          }, 1000);
        });
      },
    },
  };
</script>

```
:::
</demo-block>


<Title title="Attributes"></Title>
参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
validate-fn | 校验函数，返回boolean | function |  |
upload-api | 上传的api| function |  |
