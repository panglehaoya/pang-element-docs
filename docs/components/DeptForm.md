# DeptForm

<Title title="介绍"></Title>

根据多选框创建多个表单

<demo-block>
:::slot source
<DeptFormDemo></DeptFormDemo>
:::

通过组件的init 方法传入初始化的已选择部门和表单model

提交时通过ref校验所有表单，formModelList是所有表单数据

:::slot highlight
```html
<template>
  <div class="dept-form-demo">
    <PDeptForm
      ref="deptForm"
      :topLabel="{ label: '主责部门', width: 100 }"
      :selectWidth="300"
      :dept-list="deptList"
    />
    <el-button @click="handleClick">提交</el-button>
  </div>
</template>

<script>
  export default {
    name: "DeptFormDemo",
    data() {
      return {
        deptList: [
          {
            value: "部门1",
            code: "dept1",
          },
          {
            value: "部门2",
            code: "dept2",
          },
          {
            value: "部门3",
            code: "dept3",
          },
        ],
      };
    },
    mounted() {
      this.$refs.deptForm.init(["dept1"], [{ title: "1", remark: "说明" }]);
    },
    methods: {
      handleClick() {
        try {
          this.$refs.deptForm.validateForm();
          console.log(this.$refs.deptForm.formModelList)
        } catch (e) {
          console.error(e);
        }
      },
    },
  };
</script>


```
:::
</demo-block>

<Title title="Attributes"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
topLabel | 多选的label配置 | object |  |
selectWidth | 多选框宽度 |  number |  |
dept-list | 多选框列表 |  array |  |

