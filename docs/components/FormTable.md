# FormTable

<Title title="介绍"></Title>

在表格中添加表单，目前支持input、input-number、select、timepicker、datepicker、

<Title title="组件的使用"></Title>

<demo-block>
:::slot source
<FormTableDemo></FormTableDemo>
:::

使用json的形式对表格配置  

可以添加多行数据，点击提交后可以获取组件的数据

:::slot highlight
```html
<template>
  <div id="app">
    <PFormTable
      ref="formTable"
      :table-options="tableOptions"
      :border="true"
      @add="handleAdd"
      @clear="handleClear"
    />
    <el-button type="primary" @click="handleSubmit">提交</el-button>
  </div>
</template>

<script>
export default {
  name: "FormTableDemo",
  data() {
    return {
      tableOptions: {
        tableColumns: [
          {
              prop: "userName",
              label: "用户姓名",
              type: "input",
              rules: true,
          },
          {
              prop: "cost",
              label: "金额",
              type: "inputNumber",
              rules: true,
          },
          {
              prop: "houseType",
              label: "房屋类型",
              type: "select",
              selectList: [
                  { dictValue: "类型1", dictCode: "code1" },
                  { dictValue: "类型2", dictCode: "code2" },
                  { dictValue: "类型3", dictCode: "code3" },
              ],
          },
          {
              prop: "date",
              label: "维修时间",
              type: "datePicker",
              options: {
                  type: "month",
              },
          },
          {
              prop: "time",
              label: "选择时间",
              type: "timePicker",
          },
        ],
        tableData: [],
      },
    };
  },
  methods: {
    handleAdd() {
      this.tableOptions.tableData.push({
        userName: "",
        houseType: "code1",
        cost: 10,
        date: "",
        time: "",
      });
    },
    handleSubmit() {
      console.log(this.$refs.formTable.formModel);
    },
    handleClear() {
      this.tableOptions.tableData = [];
    },
  },
};
</script>
```
:::
</demo-block>

<Title title="Attributes"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
disabled | 是否禁用表单 | boolean | | false
tableOptions | 表格的配置选项，详见下面的配置项 |  array |  | []

<Title title="Options"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
prop | 表单属性 | string | 
label | 表头文本 | string |
type | 表单类型 | string | input、inputNumber、select、date、time | 
rules | 是否添加规则 | string | true 、false | false
options | 表单配置项 支持element ui的所有配置项