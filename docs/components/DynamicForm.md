# DynamicForm

<Title title="介绍"></Title>

使用json的形式对表单配置

<demo-block>
:::slot source
<DynamicFormDemo></DynamicFormDemo>
:::

使用json的形式对表单配置

:::slot highlight
```html
<template>
  <div>
    <PDynamicForm v-model="formModel" :form-list="formList" />
  </div>
</template>

<script>
export default {
  name: "DynamicFormDemo",
  data() {
    return {
      formList: [
        {
          type: "input",
          prop: "userName",
          label: "用户名称",
          rules: true,
        },
        {
          type: "select",
          prop: "type",
          label: "类型",
          rules: true,
          selectList: [
            { dictValue: "类型1", dictCode: "code1" },
            { dictValue: "类型2", dictCode: "code2" },
            { dictValue: "类型3", dictCode: "code3" },
          ],
        },
        {
          type: "datePicker",
          prop: "date",
          label: "时间",
          rules: true,
        },
        {
          type: "input",
          prop: "address",
          label: "用户地址",
          rules: true,
        },
      ],
      formModel: {
        userName: "",
        type: "",
        date: "",
        address: "",
      },
    };
  },
  methods: {
    handleChange(e) {
        console.log(e);
    },
  },
};
</script>

```
:::
</demo-block>

<Title title="Attributes"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
formList | 表单列表 具体配置见下表 | array |  | 
formModel | 表单model |  object |  | 

<Title title="formList Options"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
type | 表单类型 | string | input、select、date |
prop | 表单prop |  string |  |
label | 表单label |  string |  |
rules | 表单规则 |  array \| boolean |  | false | 
