# MultiLevelMenu

<Title title="介绍"></Title>

多级菜单

<Title title="组件的使用"></Title>

<demo-block>
:::slot source
<MultiLevelMenuDemo></MultiLevelMenuDemo>
:::

:::slot highlight
```html
<template>
  <div>
    <PMultiLevelMenu
      :menu-list="menuList"
      :menu-width="'500'"
      :collapse="collapse"
      @select="handleSelect"
    />
    <el-button type="primary" @click="collapse = !collapse">折叠</el-button>
  </div>
</template>

<script>
export default {
  name: "App",
  data() {
    return {
      menuList: [
        {
          index: "/",
          title: "导航1",
          icon: "el-icon-edit-outline",
          children: [
            {
              index: "/path1-1",
              title: "导航1-1",
              children: [
                {
                  index: "/path1-1-1",
                  title: "导航1-1-1",
                  children: [
                    {
                      index: "/path1-1-1-1",
                      title: "导航1-1-1-1",
                      children: [
                        {
                          index: "/path1-1-1-1-1",
                          title: "导航1-1-1-1-1",
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          index: "/path2",
          title: "导航2",
          icon: "el-icon-use",
        },
        {
          index: "/path3",
          title: "导航3",
          icon: "el-icon-folder",
        },
      ],
      collapse: false,
    };
  },
  methods: {
    handleSelect(val) {
      console.log(val);
    },
  },
};
</script>
```
:::
</demo-block>

<Title title="Attributes"></Title>

参数|说明|类型|可选值|默认值
:-|:-|:-|:-|:-|:-
menuList | 菜单列表 具体配置见下表 | array |  |
menuWidth | 菜单宽度 |  string |  | 300 |
collapse | 是否折叠 |  boolean |  | false |

<Title title="Event"></Title>

参数|说明|参数|
:-|:-|:-|:-|:-|:-
select | 点击菜单项时触发 | 菜单index |
